<?php
require_once 'core.php';

if(isset($_GET['search'])){
	$results = search($_GET['search'], $database);
	if($results){
		for($i = 0; $i < count($results); $i++){
			$results[$i]['docs_content'] = html_entity_decode($results[$i]['docs_content']);
			$results[$i]['docs_content'] = myTruncate($results[$i]['docs_content'], 350);
		}
		$temp_array['search_results'] = $results;
	}

	//Echo the template
	echo $twig->render('search.template.php', $temp_array);
}else{
	header("Location: index.php");
}
?>