<?php
	
$menu_top = $database->select('documentation_cat', '*');
$menu_top_add = $database->select('documentation_docs', '*', ['docs_cat' => '0']);
$sub_menus = [];

if($menu_top){
	for($i = 0; $i < count($menu_top); $i++){
		$search_id = $menu_top[$i]['id'];
		$sub_parent = $menu_top[$i]['doc_cat'];
		$sub_menus[$sub_parent] = $database->select('documentation_docs', '*', ['docs_cat' => $search_id]);
		$menu_top[$i]['sub_count'] = count($sub_menus[$sub_parent]);
	}
}
?>