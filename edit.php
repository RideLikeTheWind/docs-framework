<?php
include_once 'core.php';
//Edit a 'document'
if(isset($_GET['id'])){
	$doc_id = clean($_GET['id']);
	$edit_document = $database->get('documentation_docs','*', [
		'id' => $doc_id
	]);
		$edit_document['docs_content'] = html_entity_decode($edit_document['docs_content']);
		if(!$edit_document){
			header('Location: index.php');
		}
	$temp_array['document'] = $edit_document;
	
	$documentation_cat = $database->select('documentation_cat','*');
		
	$temp_array['documentation_cat'] = $documentation_cat;

	echo $twig->render('edit.template.php', $temp_array);
	
}else if(isset($_GET['new'])){
	
	$documentation_cat = $database->select('documentation_cat','*');
		
	$temp_array['documentation_cat'] = $documentation_cat;
	
	echo $twig->render('edit.template.php', $temp_array);
}else{
	header('Location: index.php');
}
?>