<?php
require 'core.php';
if(isset($_GET['id'])){
	$doc_id = clean($_GET['id']);
	$document = $database->get('documentation_docs','*', [
		'id' => $doc_id
	]);
		$document['docs_content'] = html_entity_decode($document['docs_content']);
		if(!$document){
			header('Location: index.php');
		}
	$temp_array['document'] = $document;

	//Echo the template
	echo $twig->render('document.template.php', $temp_array);
}


?>