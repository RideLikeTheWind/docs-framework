<?php
require 'core.php';

$results = $database->select('documentation_docs', '*', [
	'ORDER' => "docs_created DESC",
	"LIMIT" => 3
]);
	if($results){
		for($i = 0; $i < count($results); $i++){
			$results[$i]['docs_content'] = html_entity_decode($results[$i]['docs_content']);
			$results[$i]['docs_content'] = myTruncate($results[$i]['docs_content'], 350);
		}
		$temp_array['search_results'] = $results;
	}
	$temp_array['search_results'] = $results;

//Echo the template
echo $twig->render('home.template.php', $temp_array);

?>