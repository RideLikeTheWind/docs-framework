<?php
	
//DB Functions
// Mainly handles POST requests and standard, specific DB requests
require_once 'core.php';

if($_POST){
	
	if(isset($_POST['new_cat'])){
		
		$categories = split(',', $_POST['new_cat']);

		for($i = 0; $i< count($categories); $i++){
			$trimmed = trim($categories[$i]);
			$categories[$i] = $trimmed;
		}
		$insert = [];
		foreach($categories as $cat){
			$insert[] = ['doc_cat' => $cat];
		}
		$res = $database->insert('documentation_cat', $insert);
	}
	if($res > 0){
		header("Location: settings.php");
	}
	
	if(isset($_POST['type']) && $_POST['type'] == 'document'){
			
		
		$if_exists = $database->select('documentation_docs', ['id'], ['id' => $_POST['id']]);
			
		if($if_exists != false){
			$docs_update = [
				'docs_title' => htmlentities($_POST['docs_title'], ENT_QUOTES, 'UTF-8'),
				'docs_content' => htmlentities($_POST['docs_content'], ENT_QUOTES, 'UTF-8'),
				'docs_comment' => htmlentities($_POST['docs_comment'], ENT_QUOTES, 'UTF-8'),
				'docs_in_menu' => $_POST['docs_in_menu'],
	 		 'docs_edited' => date("Y-m-d H:i:s"),
			 'docs_cat' => $_POST['docs_cat']
			];
			$res = $database->update('documentation_docs', $docs_update, ['id' => $_POST['id']]);
			if($res >= 0){
				header("Location: document.php?id=".$_POST['id']);
			}
		}else{
			$docs_create = [
				'docs_title' => htmlentities($_POST['docs_title'], ENT_QUOTES, 'UTF-8'),
				'docs_content' => htmlentities($_POST['docs_content'], ENT_QUOTES, 'UTF-8'),
				'docs_comment' => htmlentities($_POST['docs_comment'], ENT_QUOTES, 'UTF-8'),
				'docs_in_menu' => $_POST['docs_in_menu'],
	 		 'docs_created' => date("Y-m-d H:i:s"),
			 'docs_cat' => $_POST['docs_cat']
			];
			$res = $database->insert('documentation_docs', $docs_create);
			if($res >= 0){
				header("Location: document.php?id=".$res);
			}
		}
	}
	
	
	
}

?>