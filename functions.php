<?php

// Handles GET requests amd other arbitary functions

// Thanks to PHP.net
function getCat($database) {
	$allCat = $database->select('documentation_cat', '*');
	return $allCat;
}

function clean($text)
{
	$text = strip_tags($text);
	$text = htmlspecialchars($text, ENT_QUOTES);
	
    return ($text); //output clean text
}

if(!isset($database)){
	require_once 'core.php';
}

if(isset($_GET['action'])){
	switch ($_GET['action']){
		case 'delcat':
			$delete_statement = $database->query('DELETE FROM documentation_cat WHERE id='.$_GET['id']);
			$returl = 'settings.php';
			break;
		case 'delete': 
			$delete_statement = $database->query('DELETE FROM documentation_docs WHERE id='.$_GET['id']);
			$returl = 'index.php';
			break;
	}
	header('Location: '.$returl);
}

function search($search_string, $database) {
	
	$query = $search_string;
  $query = clean($query);
	$query_string = "SELECT * FROM documentation_docs
      WHERE (`docs_title` LIKE '%".$query."%') OR (`docs_content` LIKE '%".$query."%')";
  $raw_results = $database->query($query_string)->fetchAll();
	return $raw_results;
}

// Original PHP code by Chirp Internet: www.chirp.com.au
  // Please acknowledge use of this code by including this header.
	//Modified by user Michael in comments
	
function myTruncate($string, $limit, $break=" ", $pad="  ...") {

  // return with no change if string is shorter than $limit

  if(mb_strlen($string, 'UTF-8') <= $limit) return $string;



  // is $break present between $limit and the end of the string?

  if(false !== ($breakpoint = mb_strpos($string, $break, $limit, "UTF-8"))) {

    if($breakpoint < mb_strlen($string, 'UTF-8') - 1) {

      // $string = substr($string, 0, $breakpoint) . $pad;

			$string = mb_substr($string, 0, $breakpoint, "UTF-8") . $pad;

    }

  }



	#put all opened tags into an array

	preg_match_all ( "#<([a-z]+)( .*)?(?!/)>#iU", $string, $result );

	$openedtags = $result[1];



	#put all closed tags into an array

	preg_match_all ( "#</([a-z]+)>#iU", $string, $result );

	$closedtags = $result[1];

	$len_opened = count ( $openedtags );

	# all tags are closed

	if( count ( $closedtags ) == $len_opened ) {

			return $string;

	}

	$openedtags = array_reverse ( $openedtags );

	# close tags

	for( $i = 0; $i < $len_opened; $i++ ) {

			if ( !in_array ( $openedtags[$i], $closedtags ) )

			{

					$string .= "</" . $openedtags[$i] . ">";

			}

			else

			{

					unset ( $closedtags[array_search ( $openedtags[$i], $closedtags)] );

			}

	}

	return $string;

}
?>