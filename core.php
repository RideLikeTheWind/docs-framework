<?php
session_start();

require_once 'medoo.min.php';
date_default_timezone_set('Australia/Sydney');

require_once 'Twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register();

try {
	$database = new medoo([
			'database_type' => 'mysql',
			'database_name' => 'test',
			'server' => '127.0.0.1',
			'username' => 'root'
		]);
} catch (Exception $e) {
	$err_mess[] = $e->getMessage();
}


require 'menu.php';
$temp_array = ['menu_top' => $menu_top, 'menu_top_add' => $menu_top_add, 'sub_menus' => $sub_menus, 'error' => $err_mess];
//I used TWIG because it's faster for templating
$loader = new Twig_Loader_Filesystem('templates/');
$twig = new Twig_Environment($loader, array('debug' => true));
$twig->addExtension(new Twig_Extension_Debug());
require_once 'functions.php';
?>