<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Task Docs</title>

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="includes/core.styles.css" type="text/css" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container fluid">
		
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="index.php">Task Docs</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
				  		{% for item in menu_top %}
								{% if item.sub_count > 0 %}
									<li class="dropdown">
					  				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ item.doc_cat|raw|title }} <span class="caret"></span></a>
					  		
						  	  	<ul class="dropdown-menu" role="menu">
					 					 {% for sub_items in sub_menus %}
										 		{% for items in sub_items %}
												{% if items.docs_cat == item.id %}
					  							<li><a href="document.php?id={{ items.id }}">{{ items.docs_title|raw|title }}</a></li>
												{% endif %}
												{% endfor %}
										{% endfor %}
										</ul>	
								</li>
							{% else %}
								<li><a href="#">{{ item.doc_cat|raw|title }}</a></li>
								{% endif %}
							{% endfor %}
							{% for item_add in menu_top_add %}
								<li><a href="document.php?id={{ item_add.id }}"> {{ item_add.docs_title|raw|title }}</a></li>
							{% endfor %}
		        </ul>
		      <p class="navbar-text navbar-right"><a href='settings.php'><span class="glyphicon glyphicon-cog"></span></a></p>
					<p class="navbar-text navbar-right"><a href='edit.php?new'><span class="glyphicon glyphicon-plus"></span></a></p>
		      <form class="navbar-form navbar-right" role="search" action="search.php" method="GET">
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="Search" name="search">
		        </div>
		        <button type="submit" name="submit" class="btn btn-default">Search</button>
		      </form>
					
		      <!--NAVRIGHT-->
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		
		{% block main_content %}
		{% endblock main_content %}
		
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="includes/jquery-2.1.1.min.js" type="text/javascript" charset="utf-8"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="includes/tinymce/jquery.tinymce.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="includes/scripts.js" type="text/javascript" charset="utf-8"></script>
		
		{% block script %}
		{% endblock script %}
    
  </body>
</html>
