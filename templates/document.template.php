{% extends 'index.template.php' %}

{% block main_content %}

<div class="row">
	
	<div class="col-xs-12 col-sm-11">
		
		<h1>{{ document.docs_title }}</h1>
		
		{{ document.docs_content|raw }}
		<p></p>
		{% if document.docs_edited is not empty %}
		<small>Last update: {{ document.docs_edited|raw }}
			{% if document.docs_comment is not empty %}, with comment: {{ document.docs_comment }}{% endif %}</small>
		{% endif %}
	</div>
	<div class="col-xs-12 col-sm-1">
		<a href="edit.php?id={{ document.id }}" class="btn btn-warning" role="button">Edit</a>
	</div>
</div>

{% endblock main_content %}