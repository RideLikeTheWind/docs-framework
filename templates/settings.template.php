{% extends 'index.template.php' %}

{% block main_content %}

<div class="row">
	
	<div class="col-xs-12">
		<div class="col-xs-9">
			<h1>Site Categories <small>(Parent menu items)</small></h2>
			<form name="site_cat" method="POST" action="database.php">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Comma deliminated - ie. About Us, Docs, Products" name="new_cat" id="new_cat" />
				</div>
				<button type="submit" class="btn btn-primary">Create</button>
				<span class="cat-live-load"></span>
			</form>
			<div class="spacer"></div>
			<hr />
			<div class="spacer"></div>
			<h2>Current Categories</h2>
			<ul class="list-group">
				{% if category_list %}
					{% for item in category_list %}
						<li class="list-group-item">
							<a class="remtd" href="functions.php?action=delcat&id={{ item.id }}"><span class="glyphicon glyphicon-remove"></span></a>
							{{ item.doc_cat }}
						</li>
					{% endfor %}
				{% else %}
					<p>No categories... yet!</p>
				{% endif %}
			</ul>
		</div>
		<div class="col-xs-3">
			<h3><span class="glyphicon glyphicon-question-sign"></span></h3>
			<h4>Site Categories</h4>
			<p>Create items here for parent items. Creating a page will place it on the top menu unless it is placed into a category.</p>
			<h4>Current Categories</h4>
			<p>Deleting a category will not delete the attached page, however it will make it hard to find the page unless you know the url of the page.
				Create and add each page to a new category BEFORE you delete one to make sure they don't get lost</p>
		</div>
	</div>
</div>
<div class="row">
	
</div>


{% endblock main_content %}