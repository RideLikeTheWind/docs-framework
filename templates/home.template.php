{% extends 'index.template.php' %}
{% block main_content %}

<div class="row">
	<div class="col-xs-12">
<h1>Recent posts...</h3>
		{% if search_results is not empty %}
		{% for result in search_results %}
		<div class="panel panel-warning">
			<div class="panel-heading">
				<h3 class="panel-title"><a href="document.php?id={{ result.id }}">{{ result.docs_title }}</a><small> Updated: {{ result.docs_edited }}</small></h3>
			</div>
			<div class="panel-body">{{ result.docs_content|raw }}<a href="document.php?id={{ result.id }}">[ read more ]</a></div>
		</div>
			<div class="spacer"></div>
		{% endfor %}
		{% endif %}
	</div>
	
</div>

{% endblock main_content %}