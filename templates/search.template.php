{% extends 'index.template.php' %}

{% block main_content %}

<div class="row">
	<div class="col-xs-12">
		<h1>Search Results</h1>
		{% if search_results is not empty %}
		{% for result in search_results %}
		<h3>{{ loop.index }} : <a href="document.php?id={{ result.id }}">{{ result.docs_title }}</a><small> Updated: {{ result.docs_edited }}</small></h3>
		<p>{{ result.docs_content|raw }}<a href="document.php?id={{ result.id }}">... [ read more ]</a></p>
		<hr />
		{% endfor %}
		{% else %}
		<h3>No results</h3>
		{% endif %}
	</div>
	
</div>

{% endblock main_content %}