{% extends 'index.template.php' %}

{% block script %}

	<script type="text/javascript" src="includes/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
	tinymce.init({
	    selector: "textarea",
	    plugins: [
	        "autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime table contextmenu paste"
	    ],
	    toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link"
	});
	</script>
	
{% endblock script %}

{% block main_content %}

<div class="row">
	
	<form class="form" method="post" action="database.php">
		<input type="hidden" name="type" value="document" />
		{% if document is defined %}
		<input type="hidden" name="id" value="{{ document.id }}" />
		{% endif %}
	<div class="col-xs-12 col-md-9">
		<h2>{% if document is defined %}Edit Document{% else %}New document{% endif %}</h2>
		
			<div class="form-group">
				<div class="input-group">
				<input type="text" class="form-control" placeholder="Doc Title" name="docs_title" aria-describedby="id-addon" {% if document is defined %}value="{{ document.docs_title }}"{% endif %} /><span class="input-group-addon" id="id-addon">Doc id: {% if document is defined %}{{ document.id }}{% else %}...{% endif %}</span>
			</div>
			</div>
			<div class="form-group">
		    <textarea name="docs_content" style="height:200px;" class="form-control">{{ document.docs_content }}</textarea>
			</div>
			<div class="form-group">
				<input class="form-control" type="text" name="doc_comment" placeholder="Update comment/note" value="{{ document.docs_comment }}" />
			</div>
	</div>
	<div class="col-xs-3 col-md-3">
		<h2>Settings</h2>
		<div class="form-group">
			<label for="docs_cat">Category</label>
			<select name="docs_cat" class="form-control">
				<option value="0">None (Top level page)</option>
				{% for cat in documentation_cat %}
				<option value="{{ cat.id }}" {% if document.docs_cat == cat.id %} selected="selected"{% endif %}>{{ cat.doc_cat }}</option>
				{% endfor %}
			</select>
			<small>Selecting a category will put that page under the category in the top menu. Selecting None puts the page on the menu.</small>
		</div>
		</div>
	<div class="spacer"></div>
	<div class="form-group">
	<button type="submit" class="btn btn-success" name="submit" value="submit_doc">Save!</button>
	{% if document is defined %}
	<a href="functions.php?action=delete&id={{ document.id }}" class="btn btn-danger" data-confirm="true" role="button">Delete</a> <small>(Permanent!)</small>
	{% endif %}
</div>
</div>
	</form>
</div>


{% endblock main_content %}